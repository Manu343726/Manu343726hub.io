---
layout: home
excerpt: "My portfolio and thoughts landing page. Here's where my crazy stuff is compiled."
image:
  //feature: mandelbrot.jpg
  credit: Manu Sánchez	
  creditlink: http://manu343726.github.io
---

I'm Manu Sánchez, and this is my personal website. I'm mostly a programmer, so the average stuff of this website will be programming related. 

From my portfolio to some sparse posts explaining the last crazy idea I have implemented with C++. I hope you like it.